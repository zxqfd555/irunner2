'''
sobols@: This is a nasty hack to make "Remember Me" checkbox.
Code is copied from django.contrib.auth.views.
Checking 'remember_me' has been added (see CUSTOM_CODE block)
Inspired by https://djangosnippets.org/snippets/1881/ (wrong).
'''

import django.contrib.auth.views

from django.conf import settings
# Avoid shadowing the login() and logout() views below.
from django.contrib.auth import (
    REDIRECT_FIELD_NAME, login as auth_login
)

from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpResponseRedirect
from django.shortcuts import resolve_url
from django.template.response import TemplateResponse
from django.utils.http import is_safe_url
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters

import forms as custom_forms


@sensitive_post_parameters()
@csrf_protect
@never_cache
def login(request, template_name='registration/login.html',
          redirect_field_name=REDIRECT_FIELD_NAME,
          authentication_form=custom_forms.AuthenticationFormWithRememberMe,
          current_app=None, extra_context=None):
    """
    Displays the login form and handles the login action.
    """
    redirect_to = request.POST.get(redirect_field_name,
                                   request.GET.get(redirect_field_name, ''))

    if request.method == "POST":
        form = authentication_form(request, data=request.POST)
        if form.is_valid():

            # Ensure the user-originating redirection url is safe.
            if not is_safe_url(url=redirect_to, host=request.get_host()):
                redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)

            # Okay, security check complete. Log the user in.
            auth_login(request, form.get_user())

            # <CUSTOM_CODE>
            if form.cleaned_data['remember_me']:
                request.session.set_expiry(settings.SESSION_COOKIE_AGE)
            # </CUSTOM CODE>

            return HttpResponseRedirect(redirect_to)
    else:
        form = authentication_form(request)

    current_site = get_current_site(request)

    context = {
        'form': form,
        redirect_field_name: redirect_to,
        'site': current_site,
        'site_name': current_site.name,
    }
    if extra_context is not None:
        context.update(extra_context)

    if current_app is not None:
        request.current_app = current_app

    return TemplateResponse(request, template_name, context)


def password_change(request):
    template_response = django.contrib.auth.views.password_change(
        request,
        password_change_form=custom_forms.PasswordChangeForm,
        extra_context={'disable_change_password_warning': True}
    )
    # Do something with `template_response`
    return template_response
